﻿using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player_Victory_Controller : MonoBehaviour {

    public Button Play_Button;
    public Button Quit_Button;


    public void LevelTwoScene()
    {
        //The Game Scene Loads and the game starts.
        SceneManager.LoadScene("LevelTwo");
    }


    public void LevelTreeScene()
    {
        //The Game Scene Loads and the game starts.
        SceneManager.LoadScene("LevelTree");
    }

    public void QuitScene()
    {
        //The Quit button and "You Quit From The Game" appears on the console
        Debug.Log("You Quit From The Game");
        Application.Quit();
    }
}
