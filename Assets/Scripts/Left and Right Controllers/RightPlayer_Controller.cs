﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RightPlayer_Controller : MonoBehaviour {
    // Use this for initialization
    
    public GameObject rightPlayer; //set a range
	
	// Update is called once per frame
	void Update () {

        //Default speed is = 0 on every frame
        rightPlayer.GetComponent<Rigidbody2D>().velocity = new Vector3(0f, 0f, 0f);

        if (Input.GetKey(KeyCode.UpArrow)) {
            //The velocity will change to 1 when W is pressed
            rightPlayer.GetComponent<Rigidbody2D>().velocity = new Vector3(0f, 10f, 0f);
        }
        else if (Input.GetKey(KeyCode.DownArrow)) //The else is used so the player will not be able to press both W/S at the same time
        {
            //The velocity will change to -1 when S is pressed
            rightPlayer.GetComponent<Rigidbody2D>().velocity = new Vector3(0f, -10f, 0f);
            
        }

    }

}
