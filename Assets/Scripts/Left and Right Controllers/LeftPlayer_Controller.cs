﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeftPlayer_Controller : MonoBehaviour {

	// Update is called once per frame
	void Update () {
        // Left paddle is working with mouse
        //Default speed is = 0 on every frame
        Vector3 mPosition = Input.mousePosition;
        Vector3 leftPaddle = Camera.main.ScreenToWorldPoint(mPosition);
        transform.position = new Vector3(-19f, Mathf.Clamp(leftPaddle.y, -9f, 9f));
    }
}
