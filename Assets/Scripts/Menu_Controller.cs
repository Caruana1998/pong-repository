﻿using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu_Controller : MonoBehaviour {

    public Button Next_Level_Button;
    public Button Quit_Button;


    public void LevelOne()
    {
        //The Game Scene Loads and the game starts.
        SceneManager.LoadScene("LevelOne");
    }

    public void QuitScene()
    {
        //The Quit button and "You Quit From The Game" appears on the console
        Debug.Log("You Quit From The Game");
        Application.Quit();
    }
}
