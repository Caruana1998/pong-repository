﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Scoreboard_Controller : MonoBehaviour {

    public static Scoreboard_Controller instance;

    public Text playerOneScoreText;
    public Text playerTwoScoreText;

    public int playerOneScore;
    public int playerTwoScore;

    // Use this for initialization
    void Start () {
        instance = this;
        playerOneScore = playerTwoScore = 0;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void GivePlayerOneAPoint()
    {
        playerOneScore += 1;
        playerOneScoreText.text = playerOneScore.ToString();

        if(playerOneScore > 9)
        {
            SceneManager.LoadScene("Left_Player_Victory");

        }
    }

    public void GivePlayerTwoAPoint()
    {
        playerTwoScore += 1;
        playerTwoScoreText.text = playerTwoScore.ToString();

        if (playerTwoScore > 9)
        {
            SceneManager.LoadScene("Right_Player_Victory");
        }
    }

}
