﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Ball_Controller : MonoBehaviour
{

    public Text Scoreboard;
    //set the speed to 30
    public float speed = 100;

    // Use this for initialization
    void Start()
    {
        //Start from the right and multiply the speed by the direction 
        GetComponent<Rigidbody2D>().velocity = Vector2.right * speed;
    }

    float hitFactor(Vector2 ballPosition, Vector2 batPosition, float batHeight)
    {
        return (ballPosition.y - batPosition.y) / batHeight;
    }

    void OnCollisionEnter2D(Collision2D collision)
    {

        if (collision.gameObject.name == "leftPlayer")
        {
            // Calculate hit Factor
            float y = hitFactor(transform.position, collision.transform.position, collision.collider.bounds.size.y);

            // Calculate direction, make length=1 via .normalized
            Vector2 direction = new Vector2(2, y).normalized;

            // Set Velocity with dir * speed
            GetComponent<Rigidbody2D>().velocity = direction * speed;
            
        }

        // if the rightPlayer is hit
        if (collision.gameObject.name == "rightPlayer")
        {
            // Calculating the hit Factor
            float y = hitFactor(transform.position, collision.transform.position, collision.collider.bounds.size.y);

            // Calculate direction, make length = 1 via .normalized
            Vector2 direction = new Vector2(-2, y).normalized;

            // Set Velocity with dir * speed
            GetComponent<Rigidbody2D>().velocity = direction * speed;
       
        }



        if (collision.gameObject.name == "LowerLeftObstacle")
        {
            // Calculating the hit Factor
            float y = hitFactor(transform.position, collision.transform.position, collision.collider.bounds.size.y);

            // Calculate direction, make length = 1 via .normalized
            Vector2 direction = new Vector2(2, y).normalized;

            // Set Velocity with dir * speed
            GetComponent<Rigidbody2D>().velocity = direction * speed;

        }
        if (collision.gameObject.name == "UpperLeftObstacle")
        {
            // Calculating the hit Factor
            float y = hitFactor(transform.position, collision.transform.position, collision.collider.bounds.size.y);

            // Calculate direction, make length = 1 via .normalized
            Vector2 direction = new Vector2(2, y).normalized;

            // Set Velocity with dir * speed
            GetComponent<Rigidbody2D>().velocity = direction * speed;

        }

        if (collision.gameObject.name == "LowerRightObstacle")
        {
            // Calculating the hit Factor
            float y = hitFactor(transform.position, collision.transform.position, collision.collider.bounds.size.y);

            // Calculate direction, make length = 1 via .normalized
            Vector2 direction = new Vector2(-2, y).normalized;

            // Set Velocity with dir * speed
            GetComponent<Rigidbody2D>().velocity = direction * speed;

        }
        if (collision.gameObject.name == "UpperRightObstacle")
        {
            // Calculating the hit Factor
            float y = hitFactor(transform.position, collision.transform.position, collision.collider.bounds.size.y);

            // Calculate direction, make length = 1 via .normalized
            Vector2 direction = new Vector2(-2, y).normalized;

            // Set Velocity with dir * speed
            GetComponent<Rigidbody2D>().velocity = direction * speed;

        }



        if (collision.gameObject.name == "LeftWall")
        {
            Scoreboard_Controller.instance.GivePlayerTwoAPoint();

        }

        if (collision.gameObject.name == "RightWall")
        {
            Scoreboard_Controller.instance.GivePlayerOneAPoint();

        }

    }

}
